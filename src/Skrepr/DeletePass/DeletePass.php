<?php
namespace Skrepr\DeletePass;

/**
 * Class DeletePass
 * @package Skrepr\DeletePass
 */
abstract class DeletePass
{
    /**
     * @param $deletePassId
     * @param bool $forceGenerate
     * @return mixed|string
     */
    public static function get($deletePassId, $forceGenerate = false)
    {
        $ns = new \Zend_Session_Namespace('SkreprDeletePass');
        $ns->setExpirationHops(3);

        if (null === $ns->{$deletePassId} || $forceGenerate) {
            $ns->{$deletePassId} = self::getToken();
        }

        return $ns->{$deletePassId};
    }

    /**
     * @param $pass
     * @param $deletePassId
     * @return bool
     */
    public static function check($pass, $deletePassId)
    {
        $ns = new \Zend_Session_Namespace('SkreprDeletePass');

        if (null === $ns->{$deletePassId}) {
            return false;
        }

        if ($ns->{$deletePassId} !== $pass) {
            return false;
        }

        unset($ns->{$deletePassId});

        return true;
    }

    private static function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private static function getToken($length=64){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[self::crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $token;
    }
}
